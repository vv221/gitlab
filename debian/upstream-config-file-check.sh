set -e
export LC_ALL=C.UTF-8
find config | sort > debian/upstream-config-file-list.new
if ! diff -u debian/upstream-config-file-list debian/upstream-config-file-list.new
then
  echo "---------------------\n"
  echo "Obsolete config files are:"
  echo "---------------------"
  comm -13 debian/upstream-config-file-list.new debian/upstream-config-file-list
  echo "---------------------\n"
  echo "These should be added to debian/maintscript"
  echo "---------------------"
fi
