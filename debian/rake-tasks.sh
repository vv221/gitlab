#! /bin/sh

set -e

# Note: We have to work withing some constraints.
# 1. yarn wants to be able to delete and create node_modules.
# But debian policy don't allow writing to /usr/share in postinst.
# 2. So we should run yarn commands from /var/lib/gitlab only.
# 3. But rails commands like rake assets:precompile need to be run in
# /usr/share/gitlab.
# 4. So we symlink /usr/share/gitlab/node_modules to
# /var/lib/gitlab/node_modules
# 5. Additionally even the presence of this symlink breaks yarn
# So we remove the symlink before any yarn command and add it back when
# running rake assets:precompile

# Read debian specific configuration
. /usr/lib/gitlab/scripts/gitlab-env.sh

# Make yarn command available to cssbundling-rails
export PATH=/usr/share/gitlab/bin:$PATH

# Make node_modules visible to cssbundling-rails and webpack
export NODE_PATH=node_modules

# Tell yarn to install only production dependencies
export NODE_ENV=production

# Tell cssbundling-rails not to run yarn install again
# Since yarn install can only happen from /var/lib/gitlab
# but rake assets:precompile can only happen from /usr/share/gitlab
export SKIP_YARN_INSTALL=yes

# Remove all lines from Gemfile.lock since versions from debian archive
# can be different from what upstream has locked to. We still comply
# with requirements in Gemfile
runuser -u ${gitlab_user} -- sh -c "touch ${gitlab_data_dir}/Gemfile.lock && \
truncate -s 0 ${gitlab_data_dir}/Gemfile.lock"

# Regenerate Gemfile.lock to match the versions from debian archive
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle install --local'

# Check if the db is already present
db_relations="$(LANG=C runuser -u postgres -- sh -c "psql gitlab_production -c \"\d\"" 2>&1)"
if [ "$db_relations" = "No relations found." ] || \
  [ "$db_relations" = "Did not find any relations." ]; then
  echo "Initializing database..."
  test -f ${gitlab_home}/db/structure.sql || \
  runuser -u ${gitlab_user} -- sh -c \
  "cp ${gitlab_data_dir}/db/structure.sql.template ${gitlab_data_dir}/db/structure.sql"
  runuser -u ${gitlab_user} -- sh -c \
  "touch ${gitlab_data_dir}/.gitlab_shell_secret"
  runuser -u ${gitlab_user} -- sh -c 'touch /var/lib/gitlab/secrets.yml'
  runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake db:schema:load'
else
  echo "gitlab_production database is not empty, skipping gitlab setup"
fi

runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake db:migrate'

# Restrict permissions for secret files
chmod 0700 ${gitlab_data_dir}/.gitlab_shell_secret

# This is a work around to yarn insisting on removing the node_modules directory
echo "Removing node_modules symlink as workaround for yarn classic..."
rm -f /usr/share/gitlab/node_modules

# yarn needs to be able to create a node_modules directory (we have a symlink
# in /usr/share/gitlab)
cd /var/lib/gitlab
runuser -u ${gitlab_user} -- sh -c 'install -d /var/lib/gitlab/.cache'
# Revert back to yarn classic since yarn berry won't use upstream yarn.lock
# and which can break webpack due to dependency mismatch
echo "Forcing yarn classic to use frozen-lockfile option..."
runuser -u ${gitlab_user} -- sh -c 'if [ "$(yarnpkg -v|cut -d. -f1)" = "1" ]; then \
 if ! [ -f .yarn/releases/yarn-classic.cjs ]; then truncate -s 0 .yarnrc.yml; fi; \
 else yarnpkg set version classic; fi'
echo "Installing node modules..."
runuser -u ${gitlab_user} -- sh -c 'yarnpkg --frozen-lockfile install'
# Remove write permissions of .yarn-metadata.json files
echo "Fixing permissions of .yarn-metadata.json files..."
runuser -u ${gitlab_user} -- sh -c 'if [ -d "/var/lib/gitlab/.cache/yarn/v6" ]; then \
  find /var/lib/gitlab/.cache/yarn/v6/ -name .yarn-metadata.json -perm -a=w -exec chmod 644 {} \;; fi'

# Restore node_modules symlink
echo "Restoring node_modules symlink removed earlier as workaround..."
ln -sf /var/lib/gitlab/node_modules /usr/share/gitlab

# Remaining steps should be in rails app root
cd /usr/share/gitlab

echo "Precompiling locales..."
dpkg-maintscript-helper dir_to_symlink \
 /usr/share/gitlab/locale /var/lib/gitlab/locale 16.8.2-5  gitlab -- "$@"
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake gettext:compile'

echo "Precompiling assets..."
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle exec rake tmp:cache:clear assets:precompile'

echo "Webpacking..."
# Workaround for webpack crashing with nodejs 10 - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956211
# Build assets in production mode - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956508
runuser -u ${gitlab_user} -- sh -c 'NODE_OPTIONS="--max-old-space-size=4096" webpack --config config/webpack.config.js'
