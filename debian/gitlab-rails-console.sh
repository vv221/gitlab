#! /bin/sh

set -e

# Read debian specific configuration
. /usr/lib/gitlab/scripts/gitlab-env.sh

runuser -u ${gitlab_user} -- sh -c "/usr/bin/bundle exec rails console -e production"
