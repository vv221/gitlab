#! /bin/sh

set -e

# Read debian specific configuration
. /usr/lib/gitlab/scripts/gitlab-env.sh

# Remove all lines from Gemfile.lock
runuser -u ${gitlab_user} -- sh -c "touch ${gitlab_data_dir}/Gemfile.lock && \
truncate -s 0 ${gitlab_data_dir}/Gemfile.lock"
# Regenerate Gemfile.lock
runuser -u ${gitlab_user} -- sh -c '/usr/bin/bundle install --local'
