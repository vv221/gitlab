require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
# directories without any spec.rb files: support, features, fixtures
# TODO: enable javascript tests
	spec.pattern = FileList[
'spec/config/mail_room_spec.rb'
#'spec/config/**/*_spec.rb',
#'spec/controllers/*_spec.rb',
#'spec/factories_spec.rb',
#'spec/finders/**/*_spec.rb',
#'spec/helpers/*_spec.rb',
#'spec/initializers/*_spec.rb',
#'spec/lib/**/*_spec.rb',
#'spec/routing/**/*_spec.rb',
#'spec/services/**/*_spec.rb',
#'spec/tasks/**/*_spec.rb',
#'spec/uploaders/*_spec.rb',
#'spec/views/**/*_spec.rb',
#'spec/workers/**/*_spec.rb',
] - FileList[
#'spec/services/git_push_service_spec.rb',
#'spec/services/create_deployment_service_spec.rb',
#'spec/services/ci/image_for_build_service_spec.rb',
#'spec/services/ci/process_pipeline_service_spec.rb',
#'spec/services/ci/send_pipeline_notification_service_spec.rb',
#'spec/services/merge_requests/merge_when_build_succeeds_service_spec.rb',
#'spec/services/system_note_service_spec.rb',
#'spec/controllers/registrations_controller_spec.rb',
#'spec/lib/gitlab/metrics/sampler_spec.rb',
#'spec/lib/banzai/filter/sanitization_filter_spec.rb',
#'spec/lib/gitlab/badge/build/status_spec.rb',
#'spec/lib/gitlab/database/migration_helpers_spec.rb',
#'spec/lib/gitlab/git/hook_spec.rb',
#'spec/lib/gitlab/saml/user_spec.rb',
#'spec/lib/gitlab/sherlock/line_profiler_spec.rb',
#'spec/lib/gitlab/upgrader_spec.rb',
#'spec/lib/gitlab/url_sanitizer_spec.rb',
#'spec/lib/gitlab/workhorse_spec.rb'
]
# https://gitlab.com/gitlab-org/gitlab-ce/issues/25174
end
