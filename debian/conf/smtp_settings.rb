# To enable smtp email delivery for your GitLab instance do the following:
# 1. Rename this file to smtp_settings.rb
# 2. Edit settings inside this file
# 3. Restart GitLab instance
#
# For full list of options and their values see http://api.rubyonrails.org/classes/ActionMailer/Base.html
#
# If you change this file in a Merge Request, please also create a Merge Request on https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests

if Rails.env.production?
  Rails.application.config.action_mailer.delivery_method = :sendmail
  Rails.application.config.action_mailer.default_options = {from: "#{ENV['GITLAB_EMAIL_FROM']}" || "#{Settings.gitlab['email_from']}" || ''}

  ActionMailer::Base.sendmail_settings = {}
end
