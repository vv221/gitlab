#! /bin/bash
# Using bash for ${variable@Q} option for passing quotes from command line argument to rake
set -e

# Read debian specific configuration
. /usr/lib/gitlab/scripts/gitlab-env.sh

runuser -u ${gitlab_user} -- sh -c "/usr/bin/bundle exec rake ${@@Q}"
