#! /bin/sh
# Read debian specific configuration
# Note: This file won't be present during first time installation
# The variables below that is needed for gem install will be exported
# already in early postinst
if [ -f /etc/gitlab/gitlab-debian.conf ]; then
  . /etc/gitlab/gitlab-debian.conf
fi

export DB RAILS_ENV GEM_HOME VITE_RUBY_SKIP_COMPATIBILITY_CHECK

# Read gitlab_user from gitlab-common.conf
. /etc/gitlab-common/gitlab-common.conf

# We need to set these for troubleshooting fresh installations without
# gitlab-debian.conf
export GEM_HOME=/var/lib/gitlab/.gem
export GEM_PATH=$(runuser -u ${gitlab_user} -- sh -c 'gem env gempath')

# Also read the default values
gitlab_common_defaults=/usr/lib/gitlab-common/gitlab-common.defaults
test -f ${gitlab_common_defaults} && . ${gitlab_common_defaults}

cd /usr/share/gitlab
