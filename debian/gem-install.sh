#! /bin/sh

set -e

# Initialize gitlab specific environmental variables
. /usr/lib/gitlab/scripts/gitlab-env.sh

# Read list of gems from /var/lib/gitlab/gem-install.list
# Format: gem version_constraint version
# Example entry 1: vite_rails
# Example entry 2: rails ~> 7.0.6
while read line
do
  set -- ${line}
  if ! [ -z "$2" ]; then option=-v\ \'${2}\ ${3}\' ; fi
  runuser -u ${gitlab_user} -- sh -c "if ! gem list -i $option "^${1}$" >/dev/null; then gem install $option ${1}; fi"
  unset option
done < ${gitlab_data_dir}/gem-install.list
