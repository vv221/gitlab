Source: gitlab
Section: net
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>,
 Pirate Praveen <praveen@debian.org>,
 Balasankar C <balasankarc@debian.org>,
 Sruthi Chandran <srud@debian.org>,
 Utkarsh Gupta <utkarsh@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               dh-golang,
# See #1016217 (gccgo and x/net don't work well together right now)
#               golang-any (>= 2:1.17~),
               golang-go (>= 2:1.21~),
               bc,
               golang-github-alecthomas-chroma-v2-dev,
               golang-github-aws-aws-sdk-go-dev,
               golang-github-azure-azure-storage-blob-go-dev (>= 0.10~),
               golang-github-beorn7-perks-dev,
               golang-github-client9-reopen-dev,
               golang-github-davecgh-go-spew-dev,
               golang-github-dgryski-go-rendezvous-dev,
               golang-github-disintegration-imaging-dev,
               golang-github-fzambia-sentinel-dev,
               golang-github-garyburd-redigo-dev,
               golang-github-golang-jwt-jwt-dev (>= 4.4.3~),
               golang-github-googleapis-enterprise-certificate-proxy-dev (>= 0.3~),
               golang-github-google-uuid-dev,
               golang-github-gogo-protobuf-dev,
               golang-github-golang-protobuf-1-5-dev,
               golang-github-getsentry-raven-go-dev (>= 0.2~),
               golang-github-gomodule-redigo-dev,
# protobuf-1-5
#               golang-github-grpc-ecosystem-go-grpc-prometheus-dev,
               golang-github-hashicorp-yamux-dev (>= 0.0+git20210316.a95892c~),
               golang-github-jfbus-httprs-dev,
                 golang-github-jtolds-gls-dev (>= 4.20~),
               golang-github-jpillora-backoff-dev,
               golang-github-mitchellh-copystructure-dev,
               golang-github-mitchellh-reflectwalk-dev,
               golang-github-pmezard-go-difflib-dev,
# protobuf-1-5
#               golang-github-prometheus-client-golang-dev,
                  golang-github-cespare-xxhash-dev,
#               golang-github-prometheus-client-model-dev,
#               golang-github-prometheus-common-dev,
               golang-github-ryszard-goskiplist-dev,
               golang-github-sebest-xff-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-stretchr-testify-dev (>= 1.4~),
               golang-gitlab-gitlab-org-gitaly-dev (>= 16.11~),
                 golang-github-beevik-ntp-dev,
# protobuf-1-5
#               golang-gitlab-gitlab-org-labkit-dev (>= 1.14~),
                  golang-github-oklog-ulid-dev,
#               golang-gocloud-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-net-dev (>= 1:0.19~),
               golang-golang-x-sys-dev (>= 0.0~git20180510.7dfd129~),
# protobuf-1-5
#               golang-google-genproto-dev,
               golang-honnef-go-tools-dev,
               golang-procfs-dev,
# protobuf-1-5
#               golang-protobuf-extensions-dev,
               golang-toml-dev,
               golang-websocket-dev,
# protobuf-1-5
#               golang-google-grpc-dev (>= 1.38~),
               libimage-exiftool-perl,
               golang-github-kylelemons-godebug-dev,
               golang-github-cli-browser-dev
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/gitlab.git
Vcs-Browser: https://salsa.debian.org/ruby-team/gitlab
Homepage: https://about.gitlab.com/
XS-Ruby-Versions: all
XS-Go-Import-Path: gitlab.com/gitlab-org/gitlab
Rules-Requires-Root: no

Package: gitlab
Section: contrib/net
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${shlibs:Depends}, ${misc:Depends},
 gitlab-common (>= 16.6.5~),
 ruby (>= 1:2.7~),
   rubygems-integration (>= 1.18~),
 lsb-base (>= 3.0-6),
 rake (>= 12.3.0~),
 bundler (>= 2.3.15~),
 postgresql-client,
 postgresql-contrib (>= 12~),
 dbconfig-pgsql | dbconfig-no-thanks,
 bc,
 redis-server (>= 5:7.0.15~),
# See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=956211
 nodejs (>= 12~),
 nginx | httpd,
 default-mta | postfix | exim4 | mail-transport-agent,
 openssh-client,
 bzip2,
 ucf,
 gitlab-workhorse (>= ${source:Version}),
 ruby-rails (>= 2:6.1.7.2~),
   ruby-websocket-extensions (>= 0.1.5~),
   ruby-zeitwerk (>= 2.6.1~),
 ruby-bootsnap (>= 1.18.3~),
 ruby-ipaddr (>= 1.2.5~),
# for gitlab-secret_detection
   ruby-parallel (>= 1.22~),
 ruby-responders (>= 3.0~),
 ruby-sprockets (>= 3.7~),
 ruby-view-component (>= 3.11~),
 ruby-pg (>= 1.5.6~),
 ruby-neighbor (>= 0.2.3~),
 ruby-rugged (>= 1.6~),
 ruby-grape-path-helpers (>= 2.0~),
 ruby-faraday (>= 1.0~),
 ruby-marginalia (>= 1.11.1~),
# Authorization
 ruby-declarative-policy (>= 1.1),
# Authentication libraries
 ruby-devise (>= 4.9.3~),
 ruby-bcrypt (>= 3.1.14~),
 ruby-doorkeeper (>= 5.6.6~),
 ruby-doorkeeper-openid-connect (>= 1.8.6~),
 ruby-rexml (>= 3.2.3.1~),
 ruby-saml (>= 1.17~),
 ruby-omniauth (>= 2.1~),
 ruby-omniauth-auth0 (>= 3.1~),
 ruby-omniauth-azure-activedirectory-v2 (>= 2.0~),
 ruby-omniauth-dingtalk-oauth2 (>= 1.0.1~),
 ruby-omniauth-alicloud (>= 3.0~),
 ruby-omniauth-facebook (>= 4.0~),
 ruby-omniauth-github (>= 2.0.1~),
 ruby-omniauth-gitlab (>= 1.0.2~),
 ruby-omniauth-google-oauth2 (>= 1.1~),
 ruby-omniauth-kerberos (>= 0.3.0-3~),
 ruby-omniauth-oauth2-generic (>= 0.2.2~),
 ruby-omniauth-saml (>= 2.2~),
 ruby-omniauth-twitter (>= 1.4~),
   ruby-omniauth-oauth (>= 1.2~),
 ruby-omniauth-authentiq (>= 0.3.3~),
 ruby-omniauth-openid-connect (>= 0.10~),
 ruby-openid-connect (>= 1.3~),
 ruby-omniauth-atlassian-oauth2 (>= 0.2.0~),
 ruby-rack-oauth2 (>= 1.21.3~),
 ruby-jwt (>= 2.1~),
# Spam and anti-bot protection
 ruby-recaptcha (>= 5.12~),
 ruby-akismet (>= 3.0~),
 ruby-invisible-captcha (>= 2.1~),
# Two-factor authentication
 ruby-devise-two-factor (>= 4.0.2~),
 ruby-rqrcode (>= 2.0~),
# GitLab Pages
 ruby-validates-hostname (>= 1.0.13~),
 ruby-zip (>= 2.3.2~),
# GitLab Pages letsencrypt support
 ruby-acme-client (>= 2.0.9~),
# Browser detection
 ruby-browser (>= 5.3.1~),
# OS detection for usage ping
 ohai (>= 18.1~),
# GPG
 ruby-gpgme (>= 2.0.23~),
# LDAP Auth
 ruby-omniauth-ldap (>= 2.2~),
   ruby-ntlm (>= 0.6.1~),
 ruby-net-ldap (>= 0.17.1~),
# API
 ruby-grape (>= 2.0~),
 ruby-grape-entity (>= 1.0.1~),
 ruby-rack-cors (>= 2.0.1~),
# GraphQL API
 ruby-graphql (>= 2.0.27~),
 ruby-graphiql-rails (>= 1.4.10~),
 ruby-apollo-upload-server (>= 2.1.5~),
# Used by BulkImport feature (group::import)
 ruby-graphlient (>= 0.4.0),
# Generate Fake data
 ruby-ffaker (>= 2.23~),
#
 ruby-hashie (>= 5.0~),
# Pagination
 ruby-kaminari (>= 1.2.2~),
# HAML
 ruby-hamlit (>= 2.15~),
# Files attachments
 ruby-carrierwave (>= 1.3.2~),
 ruby-mini-magick (>= 4.12~),
# for backups
 ruby-fog-aws (>= 3.18~),
# gitlab need fog-core = 2.1.0
# ruby-fog-core (>= 2.1~),
   ruby-excon (>= 0.72~),
 ruby-fog-google (>= 1.19~),
 ruby-fog-local (>= 0.8~),
 ruby-fog-aliyun (>= 0.4.0~),
 ruby-gitlab-fog-azure-rm (>= 1.9.1~),
   ruby-azure-storage-blob (>= 2.0.0-3~),
   ruby-azure-storage-common (>= 2.0.1-5~),
# for Google storage
 ruby-google-apis-core (>= 0.11.1~),
 ruby-google-apis-compute-v1 (>= 0.57~),
 ruby-google-apis-container-v1beta1 (>= 0.43~),
 ruby-google-apis-container-v1 (>= 0.43~),
 ruby-google-apis-cloudbilling-v1 (>= 0.21~),
 ruby-google-apis-cloudresourcemanager-v1 (>= 0.31~),
 ruby-google-apis-iam-v1 (>= 0.36~),
 ruby-google-apis-serviceusage-v1 (>= 0.28~),
 ruby-google-apis-sqladmin-v1beta4 (>= 0.41~),
 ruby-google-apis-androidpublisher-v3 (>= 0.34~),
# for aws storage
 ruby-unf (>= 0.1.4-2~),
   ruby-unf-ext (>= 0.0.7.4),
# Seed data
 ruby-seed-fu (>= 2.3.7~),
# Search
 ruby-elasticsearch-model (>= 7.2~),
 ruby-elasticsearch (>= 5.0.3~),
 ruby-elasticsearch-rails (>= 7.2~),
 ruby-elasticsearch-api (>= 7.13.3~),
 ruby-aws-sdk-core (>= 3.196.1~),
 ruby-aws-sdk-cloudformation (>= 1.0~),
 ruby-aws-sdk-s3 (>= 1.146.1~),
 ruby-faraday-middleware-aws-sigv4,
 ruby-typhoeus (>= 1.4~),
# Markdown and HTML processing
 ruby-html-pipeline (>= 2.14.3~),
 ruby-task-list (>= 2.3.1~),
 ruby-gitlab-markup (>= 1.9~),
 ruby-github-markup (>= 1.7~),
 ruby-commonmarker (>= 0.23.10~),
 ruby-kramdown (>= 2.3.1~),
 ruby-redcloth (>= 4.3.3~),
# rdoc is built-in with ruby
 ruby-org (>= 0.9.12-2~),
 ruby-creole (>= 0.5.0~),
 ruby-wikicloth (>= 0.8.1~),
 asciidoctor (>= 2.0.18~),
 ruby-asciidoctor-include-ext (>= 0.4~),
 ruby-asciidoctor-plantuml (>= 0.0.16~),
 ruby-asciidoctor-kroki (>= 0.8~),
 ruby-rouge (>= 4.2~),
 ruby-truncato (>= 0.7.12~),
 ruby-bootstrap-form (>= 4.2~),
 ruby-nokogiri (>= 1.16~),
 ruby-escape-utils (>= 1.2.1~),
# Calendar rendering
 ruby-icalendar,
# Diffs
 ruby-diffy (>= 3.4~),
 ruby-diff-match-patch (>= 0.1~),
# Application server
 ruby-rack (>= 2.2.7~),
 ruby-rack-timeout (>= 0.6.0~),
 puma (>= 6.3~),
 ruby-puma-worker-killer (>= 0.3.1~),
 ruby-sd-notify,
# State machine
 ruby-state-machines-activerecord (>= 0.8~),
   ruby-state-machines-activemodel (>= 0.7.1~),
# Issue tags
 ruby-acts-as-taggable-on (>= 10.0~),
# Background jobs
 ruby-sidekiq (>= 7~),
 ruby-sidekiq-cron (>= 1.12~),
 ruby-redis-namespace (>= 1.10~),
# Cron Parser
 ruby-fugit (>= 1.8.1~),
# HTTP requests
 ruby-httparty (>= 0.16.4~),
# Colored output to console
 ruby-rainbow (>= 3.0~),
# Progress bar
 ruby-progressbar (>= 1.10~),
# Linear-time regex library for untrusted regular expressions
 ruby-re2 (>= 2.6~),
# Misc
 ruby-semver-dialects (>= 2.0.2~),
 ruby-version-sorter (>= 2.3~),
# Export Ruby Regex to Javascript
 ruby-js-regex (>= 3.8~),
# User agent parsing
 ruby-device-detector,
# Redis
 ruby-redis (>= 4.7~),
 ruby-connection-pool (>= 2.4~),
# Redis session store
 ruby-redis-actionpack (>= 5.4~),
   ruby-redis-store (>= 1.10~),
# Discord integration
 ruby-discordrb-webhooks (>= 3.5~),
# JIRA integration
 ruby-jira (>= 2.3~),
# Flowdock integration
 ruby-flowdock (>= 0.7~),
   ruby-posix-spawn (>= 0.3.13~),
# Slack integration
 ruby-slack-messenger (>= 2.3.3~),
# Hangouts Chat integration
 ruby-hangouts-chat (>= 0.0.5),
# FogBugz integration
 ruby-fogbugz (>= 0.3.0~),
# Kubernetes integration
 ruby-kubeclient (>= 4.11~),
   ruby-recursive-open-struct (>= 1.1.1~),
   ruby-http (>= 4.4~),
# AI
 ruby-ruby-openai (>= 3.7~),
 ruby-circuitbox (>= 2.0~),
# Sanitize user input
 ruby-sanitize (>= 6.0.2~),
 ruby-babosa (>= 2.0~),
# Sanitizes SVG input
 ruby-loofah (>= 2.22~),
# Working with license
 ruby-licensee (>= 9.16~),
# Protect against bruteforcing
 ruby-rack-attack (>= 6.7~),
# Ace editor
 ruby-ace-rails-ap (>= 4.1~),
# Detect and convert string character encoding
 ruby-charlock-holmes (>= 0.7.5~),
# Detect mime content type from content
 ruby-ruby-magic (>= 0.6~),
# Faster blank
 ruby-fast-blank,
# Parse time & duration
 ruby-gitlab-chronic (>= 0.10.5~),
 ruby-gitlab-chronic-duration (>= 0.12~),
#
 ruby-webpack-rails (>= 0.9.10~),
# We use yarn to downlod nodejs modules and hence gitlab is in contrib
 yarnpkg (>= 1.22.19+~cs24.27.18-2+deb12u1),
 ruby-rack-proxy (>= 0.7.7~),
#
 ruby-cssbundling-rails,
 ruby-terser (>= 1.0.2~),
#
 ruby-addressable (>= 2.8~),
 ruby-tanuki-emoji (>= 0.9~),
 ruby-gon (>= 6.4~),
 ruby-request-store (>= 1.5.1~),
 ruby-virtus (>= 1.0.5-3~),
 ruby-base32 (>= 0.3.0~),
# Sentry integration
 ruby-sentry-raven (>= 3.1~),
 ruby-sentry-ruby (>= 5.17.3~),
 ruby-sentry-rails (>= 5.17.1~),
 ruby-sentry-sidekiq (>= 5.17.3~),
# PostgreSQL query parsing
 ruby-pg-query (>= 5.1~),
#
 ruby-premailer-rails (>= 1.10.3-2~),
# LabKit: Tracing and Correlation
 ruby-gitlab-labkit (>= 0.36~),
# Thrift is a dependency of gitlab-labkit, we want a version higher than 0.14.0
# because of https://gitlab.com/gitlab-org/gitlab/-/issues/321900
 ruby-thrift (>= 0.16~),
# I18n
 ruby-ruby-parser (>= 3.20~),
 ruby-whitequark-parser (>= 3.0~),
 ruby-rails-i18n (>= 7.0.9~),
 ruby-gettext-i18n-rails (>= 1.12~),
#
 ruby-batch-loader (>= 2.0.1~),
#
 ruby-tty-prompt,
# Perf bar
 ruby-peek (>= 1.1~),
# Snowplow events tracking
 ruby-snowplow-tracker (>= 0.8~),
# Metrics
 ruby-method-source (>= 1.0~),
 ruby-webrick (>= 1.8.1~),
 ruby-prometheus-client-mmap (>= 0.23~),
#
 ruby-warning (>= 1.3~),
#
 ruby-octokit (>= 8.1~),
#
 ruby-mail-room (>= 0.10.0+really0.0.24~),
#
 ruby-email-reply-trimmer (>= 0.1~),
 ruby-html2text,
#
 ruby-prof (>= 1.3~),
 ruby-stackprof (>= 0.2.23~),
 ruby-rbtrace (>= 0.4~),
 ruby-memory-profiler (>= 1.0~),
 ruby-activerecord-explain-analyze (>= 0.1~),
# OAuth
 ruby-oauth2 (>= 2.0~),
# oauth2 2.0 is installed from rubygems.org
   ruby-omniauth-oauth2 (>= 1.7.3~),
# Health check
 ruby-health-check (>= 3.0~),
# System information
 ruby-vmstat (>= 2.3~),
 ruby-sys-filesystem (>= 1.4.3~),
# NTP client
 ruby-net-ntp,
# SSH keys support
 ruby-ssh-data (>= 1.2~),
# Spamcheck GRPC protocol definitions
 ruby-spamcheck (>= 1.10.1~),
# KAS GRPC protocol definitions
 ruby-kas-grpc (>= 16.11.5~),
#
 ruby-grpc (>= 1.42~),
#
 ruby-google-protobuf (>= 3.19.4~),
#
 ruby-toml-rb (>= 2.2~),
# Feature toggles
 ruby-flipper (>= 0.26.2~),
 ruby-flipper-active-record (>= 0.26.2~),
 ruby-flipper-active-support-cache-store (>= 0.26.2~),
 ruby-unleash (>= 3.2.2~),
 ruby-gitlab-experiment (>= 0.9.1~),
# Structured logging
 ruby-lograge (>= 0.10~),
 ruby-grape-logging (>= 1.8~),
# DNS Lookup
 ruby-gitlab-net-dns (>= 0.9.2~),
# Countries list
 ruby-countries (>= 4.0~),
 ruby-retriable (>= 3.1.2~),
# LRU cache
 ruby-lru-redux,
 ruby-erubi (>= 1.9~),
 ruby-mail (>= 2.8.1),
# File encryption
 ruby-lockbox (>= 1.3~),
# Email validation
 ruby-valid-email,
# JSON
 ruby-jsonb-accessor (>= 1.3.10~),
 ruby-json (>= 2.6.3~),
 ruby-json-schema (>= 2.8.1-2~),
 ruby-json-schemer (>= 0.2.12~),
 ruby-oj (>= 3.14.1-2~),
 ruby-oj-introspect(>= 0.7.1-3~),
 ruby-multi-json (>= 1.14.1~),
 ruby-yajl (>= 1.4.3~),
 ruby-webauthn (>= 3.0~),
 ruby-parslet,
 ruby-ipynbdiff (>= 0.4.7~),
 ruby-ed25519 (>= 1.3~),
# Vulnerability advisories
 ruby-cvss-suite,
# Apple plist parsing
 ruby-cfpropertylist (>= 3.0~),
 ruby-app-store-connect,
# For phone verification
 ruby-telesign (>= 2.2.4~),
# for ruby 3.1
   ruby-net-http-persistent (>= 4.0~),
 ruby-telesignenterprise,
 ruby-duo-api,
# for google-cloud-storage
 ruby-digest-crc,
# packaged node modules - all node packages are not packaged yet
 node-rails-actioncable,
 node-autosize (>= 4.0.2~dfsg1-5~),
 node-axios (>= 0.17.1~),
 node-babel7,
 node-babel-loader (>= 8.0~),
 node-babel-plugin-lodash,
 node-bootstrap,
 node-brace-expansion (>= 1.1.8~),
 node-cache-loader (>= 4.1~),
 node-clipboard (>= 2.0.8~),
 node-compression-webpack-plugin (>= 3.0.1~),
 node-copy-webpack-plugin (>= 5.0~),
 node-core-js (>= 3.2.1~),
 node-cron-validator,
 node-css-loader (>= 5.0~),
# node-d3 includes d3-sankey
 node-d3 (>= 5.16~),
 node-d3-selection (>= 1.2~),
 node-dateformat (>= 5.0.1~),
 node-deckar01-task-list (>= 2.2.1),
 node-dompurify (>= 2.3.6~),
 node-exports-loader (>= 0.7~),
 node-imports-loader (>= 0.8~),
 node-file-loader (>= 5.0~),
 node-font-awesome,
 node-fuzzaldrin-plus (>= 0.5~),
 node-glob (>= 7.1.6~),
 node-jed (>= 1.1.1-2~),
 node-jquery (>= 3.5~),
 node-jquery-ujs,
# Broken
# node-jquery.waitforimages,
 node-js-cookie (>= 3.0~),
 node-js-yaml (>= 3.13.1~),
 node-jszip,
 node-jszip-utils (>= 0.0.2+dfsg-2~),
 node-katex (>= 0.13~),
 node-lodash (>= 4.17.21+dfsg+~cs8.31.198.20210220-9~bpo11+2),
 node-marked (>= 0.3~),
# node-mermaid (>= 8.13.10~),
 node-minimatch,
 node-miragejs,
 node-mousetrap,
 node-pdfjs-dist,
# Include node-pikaday only after @gitlab/ui is accepted
# node-pikaday (>= 1.8.0-2~),
 node-popper.js (>= 1.16.1~),
 node-postcss (>= 8.4.5~),
 node-prismjs (>= 1.6~),
 node-prosemirror-markdown (>= 1.5.2~),
 node-prosemirror-model (>= 1.16.1~),
 node-prosemirror-state (>= 1.3.4~),
 node-prosemirror-view,
 node-raven-js,
 node-raw-loader (>= 4.0~),
 node-style-loader (>= 1.0~),
 node-three-orbit-controls (>= 82.1.0-3~),
 node-three-stl-loader (>= 1.0.4~),
 node-timeago.js (>= 4.0~),
 node-underscore (>= 1.9~),
 node-url-loader (>= 3.0~),
 node-uuid (>= 8.1~),
 node-vue (>= 2.6.10~),
 node-vue-resource (>= 1.5.1~),
# Blocked by #927254
# node-vue-router,
 webpack (>= 4.43~),
 node-webpack-stats-plugin,
 node-worker-loader (>= 2.0~),
 node-xterm,
 node-yaml,
# using yarn install for remaining node modules as it is in contrib
# node-babel-core,
# node-babel-eslint,
# node-babel-loader,
# node-babel-plugin-transform-define,
# node-babel-preset-latest,
# node-babel-preset-stage-2,
# node-debug (>= 3.1.0~),
# node-katex,
# node-marked,
# gitlab-sidekiq was failing without puma
 puma
Recommends: certbot,
 gitaly (>= 16.11~),
 openssh-server
Conflicts: libruby2.5
Description: git powered software platform to collaborate on code (non-omnibus)
 gitlab provides web based interface to host source code and track issues.
 It allows anyone for fork a repository and send merge requests. Code review
 is possible using merge request workflow. Using groups and roles project
 access can be controlled.
 .
 Unlike the official package from GitLab Inc., this package does not use
 omnibus.
 .
 Note: Currently this package is in contrib because it uses yarn to install
 some of its front end dependencies.

Package: gitlab-workhorse
Section: net
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: unloads Git HTTP traffic from the GitLab Rails app (Unicorn)
 gitlab-workhorse was designed to unload Git HTTP traffic from the GitLab Rails
 app (Unicorn) to a separate daemon. It also serves 'git archive' downloads for
 GitLab. All authentication and authorization logic is still handled by the
 GitLab Rails app.
 .
 Architecture: Git client -> NGINX -> gitlab-workhorse (makes auth request to
 GitLab Rails app) -> git-upload-pack
