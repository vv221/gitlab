# golang-archive-zip

This is a fork of the standard library's `archive/zip` as of `go1.17.3` with https://go-review.googlesource.com/c/go/+/357489 applied to fix https://github.com/golang/go/issues/48374.

This fork also works with `go1.16`.

